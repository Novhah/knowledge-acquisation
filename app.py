from flask import Flask, render_template, jsonify
from flask_cors import CORS
import requests
from flask import request
import bs4 as BeautifulSoup
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
import numpy as np
import random
import pickle
import json
import spacy
import re

def applyPOSTag(text):
    word = []
    POS = []
    global_list = []
    nlp = spacy.load('fr_core_news_sm')
    doc = nlp(text)
    y = 0
    for token in doc:
        word+=[token]
        POS+=[token.pos_]
        global_list += [(token.text,token.pos_,y)]
        y+=1
    return word , POS, global_list

def generateFeatures(dataset_list):
    data_features_list = []
    stop_words_file = open("ressources/stopwords.txt", "r")
    stop_words_list  = stop_words_file.read().splitlines()
    stop_words_file.close()
    for data in dataset_list:
        cur = data
        if cur[2] == 0:
            previousText = "null"
            previousPos = "null"
            previousLen = "null"
            previousCapitalize = -1
            previousStopWord = -1
        else:
            prv = dataset_list[cur[2]-1]
            previousText = prv[0]
            previousPos = prv[1]
            previousLen = len(prv[0])
            previousCapitalize = int(prv[0].isupper())
            previousStopWord = int((prv[0] in stop_words_list))

        if cur[2] == len(dataset_list)-1:
            nextText = "null"
            nextPos = "null"
            nextLen = "null"
            nextCapitalize = -1
            nextStopWord = -1
        else:
            nxt = dataset_list[cur[2]+1]
            nextText = nxt[0]
            nextPos = nxt[1]
            nextLen = len(nxt[0])
            nextCapitalize = int(nxt[0].isupper())
            nextStopWord = int((nxt[0] in stop_words_list))
        currentText = cur[0]
        currentPos = cur[1]
        currentLen = len(cur[0])
        currentCapitalize = int(cur[0].isupper())
        currentStopWord = int((cur[0] in stop_words_list))

        obj = {}
        obj["previousText"] = previousText
        obj["currentText"] = currentText
        obj["nextText"] = nextText

        obj["previousPos"] = previousPos
        obj["currentPos"] = currentPos
        obj["nextPos"] = nextPos

        obj["previousLen"] = previousLen
        obj["currentLen"] = currentLen
        obj["nextLen"] = nextLen

        obj["previousCapitalize"] = previousCapitalize
        obj["currentCapitalize"] = currentCapitalize
        obj["nextCapitalize"] = nextCapitalize

        obj["previousStopWord"] = previousStopWord
        obj["currentStopWord"] = currentStopWord
        obj["nextStopWord"] = nextStopWord

        data_features_list += [obj]

    return data_features_list

def vectorize_data_features(data_features_list):
    pickle_in = open("ressources/vectorizer.pickle","rb")
    v = pickle.load(pickle_in)
    feature_name = v.feature_names_
    X = v.transform(data_features_list)
    return X

def predictText(texte):
    global selectedClassifier
    res=[]
    word, pos, global_list = applyPOSTag(texte)
    data_features_list = generateFeatures(global_list)
    X = vectorize_data_features(data_features_list)
    pickle_in = open("ressources/"+selectedClassifier+".pickle","rb")
    clf = pickle.load(pickle_in)
    for i in range (0,len(X)):
        obj = {}
        obj["word"] = word[i]
        obj["classe"] = clf.predict([X[i]])
        res += [obj]
    return res

app = Flask(__name__, template_folder='templates')
CORS(app, origins="*")

@app.route('/classifier', methods=["POST"])
def change_classifier():
    global selectedClassifier
    selectedClassifier = request.json['value']
    return jsonify({'data': 'success', 'status': 200, 'current': selectedClassifier})

@app.route('/', methods=["POST", "GET"])
def index():
    if request.method == 'POST':
        texte = request.form['textarea']
        predictedText = predictText(texte)
        data = ""
        classList = ["I-ORG", "I-PER", "I-MISC", "I-LOC"]
        for res in predictedText :
            if res["classe"] in classList:
                data += ' <span class="'+str(res["classe"][0])+'">'+str(res["word"])+'</span>'
            else:
                data += ' '+str(res["word"])
        return render_template('plainUpload.html' , data=data)
    else:
        return render_template('plainUpload.html', data="")

@app.route('/file', methods=["POST", "GET"])
def file_route():
    if request.method == 'POST':
        f = request.files['file']
        predictedText = predictText(f.read().decode("utf-8"))
        data = ""
        classList = ["I-ORG", "I-PER", "I-MISC", "I-LOC"]
        for res in predictedText :
            if res["classe"] in classList:
                data += ' <span class="'+str(res["classe"][0])+'">'+str(res["word"])+'</span>'
            else:
                data += ' '+str(res["word"])
        return render_template('fileUpload.html', data=data)
    else:
        return render_template('fileUpload.html')

@app.route('/url', methods=["GET", "POST"])
def url_route():
    if request.method == 'POST':
        url = request.form['html-text']
        req = requests.get(url)

        soup = BeautifulSoup.BeautifulSoup(req.text)

        #walker(soup)
        tags = soup.find_all(["p", "span", "h1", "h2", "h3", "h4", "a", "span"])

        data = soup.get_text()

        data = ""
        for item in tags:
            data += " "+str(item.string)

        data = re.sub('<[^<]+?>', '', data)
        predictedText = predictText(data)
        data = ""
        classList = ["I-ORG", "I-PER", "I-MISC", "I-LOC"]

        for res in predictedText :
            if res["classe"] in classList:
                data += ' <span class="'+str(res["classe"][0])+'">'+str(res["word"])+'</span>'
            else:
                data += ' '+str(res["word"])

        return render_template('urlUpload.html' , html_text=data)
    else:
        return render_template('urlUpload.html' , html_text="")

@app.route('/classifier_select', methods=["GET"])
def get_classifier():
    global selectedClassifier
    return jsonify({'data': selectedClassifier, 'status': 200})

if __name__ == "__main__":
    global selectedClassifier
    selectedClassifier = "RandomForestclassifier"
    app.run()
