$(document).ready(function() {
  $.ajax({
    method: "GET",
    url: "http://127.0.0.1:5000/classifier_select",
    success: function(result){
      let options = $("#classifier-select").find("option")
      options.each(function(index) {
        if($(this).val() == result.data) {
          $(this).attr("selected", "selected")
        } else {
          $(this).removeAttr("selected")
        }
      })
    },
    error: function(error) {
      console.log(error)
    }
  });

  $("#classifier-select").on("change", function() {
      let v = $(this).val()
      let data = {
        value: v
      }

      $.ajax({
        method: "POST",
        url: "http://127.0.0.1:5000/classifier",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(result){
          console.log(result)
        },
        error: function(error) {
          console.log(error)
        }
      });
  })
})
