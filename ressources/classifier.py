from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn import svm
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import precision_score
import numpy as np
import random
import pickle
import json
import spacy

def translatePOS(pos):
    if  'PRO' in pos:
        pos = 'PRON'
    elif 'VER' in pos:
        pos = 'VERB'
    elif 'DET:POS' in pos:
        pos = 'PRON'
    elif 'DET:ART' in pos:
        pos = 'DET'
    elif 'PRP' in pos:
        pos = 'ADP'
    elif 'KON' in pos:
        pos = 'CCONJ'
    elif 'INT' in pos:
        pos = 'X'
    elif 'NOM' in pos:
        pos = 'NOUN'
    elif 'NAM' in pos:
        pos = 'NOUN'
    elif 'NUM' in pos:
        pos = 'NUM'
    elif 'PUN' in pos:
        pos = 'PUNCT'
    elif 'ADJ' in pos:
        pos = 'ADJ'
    elif 'ADV' in pos:
        pos = 'ADV'
    elif 'ABR' in pos:
        pos = 'X'
    elif 'SENT' in pos:
        pos = 'PUNCT'
    elif 'SYM' in pos:
        pos = 'X'
    return pos

def create_global_data_list(data_file):
    word = [] # liste  des mots
    POS = [] # liste des POS tag
    classe = [] # liste des classes
    global_list = [] # liste contenant des tuples avec toutes les infos precedentes
    y=0
    dataset = open(data_file, "r")
    lines = dataset.readlines()
    for i in range(0,len(lines)):
       sentence = lines[i]
       if(sentence != ""):
           split_sentence = sentence.split()
           for split_entity in split_sentence:
               array = split_entity.split("|")
               POS_trans = translatePOS(array[1])

               word += [array[0]]
               POS += [POS_trans]
               classe += [array[2]]
               global_list +=[(array[0], POS_trans, array[2], y)]
               y+=1
    dataset.close()

    return word, POS, classe, global_list

def split_dataset(global_list, n):
    name_entity_list = []
    uname_entity_list = []
    for entity in global_list:
        if(entity[2] == 'O'):
            uname_entity_list += [entity]
        else:
            name_entity_list += [entity]
    name_entity_set = random.sample(name_entity_list, n)
    uname_entity_set = random.sample(uname_entity_list, n)

    return name_entity_set, uname_entity_set

def generateFeatures(dataset_list, global_list, filename):
    Y = []
    data_features_list = []
    stop_words_file = open("stopwords.txt", "r")
    stop_words_list  = stop_words_file.read().splitlines()
    stop_words_file.close()
    for data in dataset_list:
        cur = data

        if cur[3] == 0:
            previousText = "null"
            previousPos = "null"
            previousLen = "null"
            previousCapitalize = -1
            previousStopWord = -1
        else:
            prv = global_list[cur[3]-1]
            previousText = prv[0]
            previousPos = prv[1]
            previousLen = len(prv[0])
            previousCapitalize = int(prv[0].isupper())
            previousStopWord = int((prv[0] in stop_words_list))

        if cur[3] == len(global_list)-1:
            nextText = "null"
            nextPos = "null"
            nextLen = "null"
            nextCapitalize = -1
            nextStopWord = -1
        else:
            nxt = global_list[cur[3]+1]
            nextText = nxt[0]
            nextPos = nxt[1]
            nextLen = len(nxt[0])
            nextCapitalize = int(nxt[0].isupper())
            nextStopWord = int((nxt[0] in stop_words_list))
        currentText = cur[0]
        currentPos = cur[1]
        currentLen = len(cur[0])
        currentCapitalize = int(cur[0].isupper())
        currentStopWord = int((cur[0] in stop_words_list))
        Y+= [cur[2]]

        obj = {}
        obj["previousText"] = previousText
        obj["currentText"] = currentText
        obj["nextText"] = nextText

        obj["previousPos"] = previousPos
        obj["currentPos"] = currentPos
        obj["nextPos"] = nextPos

        obj["previousLen"] = previousLen
        obj["currentLen"] = currentLen
        obj["nextLen"] = nextLen

        obj["previousCapitalize"] = previousCapitalize
        obj["currentCapitalize"] = currentCapitalize
        obj["nextCapitalize"] = nextCapitalize

        obj["previousStopWord"] = previousStopWord
        obj["currentStopWord"] = currentStopWord
        obj["nextStopWord"] = nextStopWord

        data_features_list += [obj]

    json_data = json.dumps(data_features_list)
    fichier = open(filename + ".json", "a")
    fichier.write(json_data)
    fichier.close()

    return data_features_list, Y

def vectorize_data_features(data_features_list):
    v = DictVectorizer(sparse=False)
    X = v.fit_transform(data_features_list)
    pickle_out = open("vectorizer.pickle","wb")
    pickle.dump(v, pickle_out)
    pickle_out.close()
    return X

word, POS, classe, global_list = create_global_data_list("aijwikinerfrwp2")
name_entity_set, uname_entity_set = split_dataset(global_list, 5000)
data_features_list, Y = generateFeatures(name_entity_set+uname_entity_set, global_list ,'features')
X = vectorize_data_features(data_features_list)


print("#################Random forest CLassifier #################")
clf1 = RandomForestClassifier(random_state=0)
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size = 0.80, test_size = 0.20 ,random_state=0)
clf1.fit(X_train, Y_train)
Y_pred= clf1.predict(X_test)
print('macro prec:',precision_score(Y_test, Y_pred, average='macro', zero_division=1))
print('micro prec:',precision_score(Y_test, Y_pred, average='micro', zero_division=1))

pickle_out = open("RandomForestclassifier.pickle","wb")
pickle.dump(clf1, pickle_out)
pickle_out.close()

print("#################Gaussian NAive BAyes CLassifier #################")
clf3 = GaussianNB()
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size = 0.80, test_size = 0.20 ,random_state=0)
clf3.fit(X_train, Y_train)
Y_pred= clf3.predict(X_test)
print('macro prec:',precision_score(Y_test, Y_pred, average='macro', zero_division=1))
print('micro prec:',precision_score(Y_test, Y_pred, average='micro', zero_division=1))


pickle_out = open("GaussianNBclassifier.pickle","wb")
pickle.dump(clf3, pickle_out)
pickle_out.close()


print("#################SVM CLassifier #################")
#clf2 = svm.SVC()
#X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size = 0.80, test_size = 0.20 ,random_state=0)
#clf2.fit(X_train, Y_train)
#Y_pred= clf2.predict(X_test)
#print('macro prec:',precision_score(Y_test, Y_pred, average='macro', zero_division=1))
#print('micro prec:',precision_score(Y_test, Y_pred, average='micro', zero_division=1))

#pickle_out = open("SVMclassifier.pickle","wb")
#pickle.dump(clf2, pickle_out)
#pickle_out.close()


print ("fin")
