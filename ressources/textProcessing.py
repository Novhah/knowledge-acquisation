#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 09:28:44 2020

@author: 21504712
"""
#Traitement du fichier à traiter.
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
import numpy as np
import random
import pickle
import json
import spacy

def applyPOSTag(text):
    word = []
    POS = []
    global_list = []
    nlp = spacy.load('fr_core_news_sm')
    doc = nlp(text)
    y = 0
    for token in doc:
        word+=[token]
        POS+=[token.pos_]
        global_list += [(token.text,token.pos_,y)]
        y+=1
    return word , POS, global_list

def generateFeatures(dataset_list):
    data_features_list = []
    stop_words_file = open("stopwords.txt", "r")
    stop_words_list  = stop_words_file.read().splitlines()
    stop_words_file.close()
    for data in dataset_list:
        cur = data
        if cur[2] == 0:
            previousText = "null"
            previousPos = "null"
            previousLen = "null"
            previousCapitalize = -1
            previousStopWord = -1
        else:
            prv = dataset_list[cur[2]-1]
            previousText = prv[0]
            previousPos = prv[1]
            previousLen = len(prv[0])
            previousCapitalize = int(prv[0].isupper())
            previousStopWord = int((prv[0] in stop_words_list))

        if cur[2] == len(dataset_list)-1:
            nextText = "null"
            nextPos = "null"
            nextLen = "null"
            nextCapitalize = -1
            nextStopWord = -1
        else:
            nxt = dataset_list[cur[2]+1]
            nextText = nxt[0]
            nextPos = nxt[1]
            nextLen = len(nxt[0])
            nextCapitalize = int(nxt[0].isupper())
            nextStopWord = int((nxt[0] in stop_words_list))
        currentText = cur[0]
        currentPos = cur[1]
        currentLen = len(cur[0])
        currentCapitalize = int(cur[0].isupper())
        currentStopWord = int((cur[0] in stop_words_list))

        obj = {}
        obj["previousText"] = previousText
        obj["currentText"] = currentText
        obj["nextText"] = nextText

        obj["previousPos"] = previousPos
        obj["currentPos"] = currentPos
        obj["nextPos"] = nextPos

        obj["previousLen"] = previousLen
        obj["currentLen"] = currentLen
        obj["nextLen"] = nextLen

        obj["previousCapitalize"] = previousCapitalize
        obj["currentCapitalize"] = currentCapitalize
        obj["nextCapitalize"] = nextCapitalize

        obj["previousStopWord"] = previousStopWord
        obj["currentStopWord"] = currentStopWord
        obj["nextStopWord"] = nextStopWord

        data_features_list += [obj]

    return data_features_list

def vectorize_data_features(data_features_list):
    pickle_in = open("vectorizer.pickle","rb")
    v = pickle.load(pickle_in)
    feature_name = v.feature_names_
    X = v.transform(data_features_list)
    return X

def predictText(texte):
    res=[]
    word, pos, global_list = applyPOSTag(texte)
    data_features_list = generateFeatures(global_list)
    X = vectorize_data_features(data_features_list)
    pickle_in = open("classifier.pickle","rb")
    clf = pickle.load(pickle_in)
    for i in range (0,len(X)):
        obj = {}
        obj["word"] = word[i]
        obj["classe"] = clf.predict([X[i]])
        res += [obj]
    return res

res = predictText("Changement radical de doctrine. Si le président américain a pendant un temps semblé prendre un peu à la légère la menace du Covid-19, ce n'est clairement plus du tout le cas. Dans la nuit de mercredi à jeudi, en effet, Donald Trump a annoncé la suspension pour trente jours de l'entrée aux Etats-Unis de tout étranger ayant séjourné en Europe afin d'endiguer la pandémie de nouveau coronavirus. Une décision choc, qui n'a évidemment pas manqué de déclencher une nouvelle tempête sur les marchés financiers déjà très agités depuis plusieurs semaines.")
for obj in res :
    print(obj)
