from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
import numpy as np
import random
import pickle
import json


pipe = make_pipeline(
    StandardScaler(),
    LogisticRegression(random_state=0)
)

# lecture et decoupage du fichier en ligne
dataset = open("aijwikinerfrwp2", "r")
lines = dataset.readlines()

# Variables globales 
word = [] # liste  des mots 
POS = [] # liste des POS tag
classe = [] # liste des classes
global_list = [] # liste contenant des tuples avec toutes les infos precedentes


y = 0;
for i in range(len(lines)):
    sentence = lines[i]
    if(sentence != ""):
        split_sentence = sentence.split()
        for split_entity in split_sentence:
            array = split_entity.split("|")
            word += [array[0]]
            POS += [array[1]]
            classe += [array[2]]
            global_list += [(array[0], array[1], array[2], y)]
            y+=1
            
print(set(POS))
name_entity_list = []
uname_entity_list = []
for entity in global_list:
    if(entity[2] == 'O'):
        uname_entity_list += [entity]
    else:
        name_entity_list += [entity]
        
select_name_entity = random.sample(name_entity_list, 10000)
select_uname_entity = random.sample(uname_entity_list, 10000)

print(len(select_name_entity))
print(len(select_uname_entity))
            
###############################################################################
#   TP2 #Exercice 2
###############################################################################

stop_words_file = open("stopwords.txt", "r")
stop_words_list  = stop_words_file.read().splitlines()

"""
Prend une liste d'entité (tuple similaire au contenue de global_list) et renvoie X et Y.
Avec X qui est la liste de nos features vectorisées et Y qui est une liste de la classe de chaque feature.
On a Y[i] est la classe associé au token courant de la feature X[i].
"""
def store_json(entity_list, filename):
    X = [] # vecteur représentant nos features (encodage des features en nb réel)
    Y = [] # liste des Classes de chaque mot
    data = [] # dictionnaire de feature
    
    # Création de notre liste de features (dictionnaires)
    for entity in entity_list:
        
        cur = entity
        
        if entity[3]-1 < 0:
            previousText = "null"
            previousPos = "null"
            previousLen = "null"
            previousCapitalize = -1
            previousStopWord = -1
        else:
            prv = global_list[entity[3]-1]
            previousText = prv[0]
            previousPos = prv[1]
            previousLen = len(prv[0])
            previousCapitalize = int(prv[0].isupper())
            previousStopWord = int((prv[0] in stop_words_list))
            
        if entity[3]+1 >= len(global_list):
            nextText = "null"
            nextPos = "null"
            nextLen = "null"
            nextCapitalize = -1
            nextStopWord = -1   
        else:
            nxt = global_list[entity[3]+1] 
            nextText = nxt[0]
            nextPos = nxt[1]
            nextLen = len(nxt[0])
            nextCapitalize = int(nxt[0].isupper())
            nextStopWord = int((nxt[0] in stop_words_list))
        
        currentText = cur[0]
        currentPos = cur[1]
        currentLen = len(cur[0])
        currentCapitalize = int(cur[0].isupper())
        currentStopWord = int((cur[0] in stop_words_list))
        Y += [cur[2]] #stockage de la classe
        
        obj = {}
        obj["previousText"] = previousText
        obj["currentText"] = currentText
        obj["nextText"] = nextText
        
        obj["previousPos"] = previousPos
        obj["currentPos"] = currentPos
        obj["nextPos"] = nextPos
        
        obj["previousLen"] = previousLen
        obj["currentLen"] = currentLen
        obj["nextLen"] = nextLen
        
        obj["previousCapitalize"] = previousCapitalize
        obj["currentCapitalize"] = currentCapitalize
        obj["nextCapitalize"] = nextCapitalize
        
        obj["previousStopWord"] = previousStopWord
        obj["currentStopWord"] = currentStopWord
        obj["nextStopWord"] = nextStopWord
    
        data += [obj]

    # inscription des données dans un fichier JSON.
    json_data = json.dumps(data)
    fichier = open(filename + ".json", "a")
    fichier.write(json_data)
    fichier.close()
    
    # vectorisation des features
    #v = DictVectorizer(sparse=False)
    pickle_in = open("vectorizer.pickle","rb")
    v = pickle.load(pickle_in)

    X = v.fit_transform(data)

    #dump vectorizer        
    #pickle_out = open("vectorizer.pickle","wb")
    #pickle.dump(v, pickle_out)
    #pickle_out.close()


    return X,Y

"""
Xname,Yname = store_json(select_name_entity, "name_entity")
Xuname,Yuname = store_json(select_uname_entity, "uname_entity")
X = np.concatenate((Xname, Xuname))
Y = Yname + Yuname
"""

#X, Y = store_json(select_name_entity + select_uname_entity, "feature")
#clf = RandomForestClassifier(random_state=0) #notre classifier.

# on creer nos ensemble d'apprentisage et nos ensemble de test

#X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size = 0.80, test_size = 0.20 ,random_state=0)
#clf.fit(X_train, Y_train) # entrainement du classifier

#print(clf.predict([X[50]]))
#print(Y[50])



#with open("feature.json") as json_data:
 #   data = json.load(json_data)


#dump classifier
#pickle_out = open("classifier.pickle","wb")
#pickle.dump(clf, pickle_out)
#pickle_out.close()

print("finished")

#loaded classifier
pickle_in = open("classifier.pickle","rb")
clf2 = pickle.load(pickle_in)


print(clf2.predict([X[50]]))
print(Y[50])

###############################################################################
#  Partie Preliminaire
###############################################################################

#TP1 prise en main du classifier
"""
int y = 0;
for i in range(100):
    sentence = lines[i]
    if(sentence != ""):
        split_sentence = sentence.split()
        for split_entity in split_sentence:
            array = split_entity.split("|")
            word += [array[0]]
            POS += [array[1]]
            classe += [array[2]]
            global_list += [(array[0], array[1], array[2], y)]
            y+=1
clf = RandomForestClassifier(random_state=0)

word_train, word_test, classe_train, classe_test = train_test_split(word, classe, random_state=0)
pipe.fit(word_train, classe_train)
"""

# TP2 Début

#Q1)
"""
renvoie un dictionnaire contenant les frequences de chaque classes     
"""
def get_classe_freq(classe):
    dicoClasse = {}
    res = []
    for c in classe:
        if c in dicoClasse:
            dicoClasse[c] += 1
        else:   
            dicoClasse[c] = 1
    for key in dicoClasse:
        dicoClasse[key] = [dicoClasse[key] / len(classe)]
    return dicoClasse 

#Q2)
    
"""
renvoie un dictionnaire contenant les frequences de chaque mots
"""
def get_word_freq(word):
    dicoWord = {}
    res=[]
    for w in word:
        if w in dicoWord:
            dicoWord[w] += 1
        else:
            dicoWord[w] = 1
    for key in dicoWord:
        dicoWord[key] = [dicoWord[key] / len(word)]
    return dicoWord

"""
recupere dans un fichier les 200 mots les plus frequents dans le texte.
"""
def get_top_200_word(word):    
    word_freq = get_word_freq(word)
    sort_word_freq = sorted(word_freq.items(), key = lambda kv:(kv[1], kv[0]))
    top_200_word = sort_word_freq[-200:]
    fichier = open("top_200_word.txt", "a")
    for word in top_200_word:
        fichier.write(word[0] + "\n")
    fichier.close()
#get_top_200_word(word)

#Q3)
    
"""
renvoie un dictionnaire contenant les frequences de chaque POS
"""
def get_pos_freq(pos):
    dicoPos = {}
    res=[]
    for p in pos:
        if p in dicoPos:
            dicoPos[p] += 1
        else:
            dicoPos[p] = 1
    for key in dicoPos:
        dicoPos[key] = [dicoPos[key] / len(pos)]
    return dicoPos

"""
recupere dans un fichier les 20 POS les plus frequents dans le texte.
"""
def get_top_20_pos(pos):
    pos_freq = get_pos_freq(pos)
    sort_pos_freq = sorted(pos_freq.items(), key = lambda kv:(kv[1], kv[0]))
    top_20_pos = sort_pos_freq[-20:]
    fichier = open("top_20_pos.txt", "a")
    for pos in top_20_pos:
        fichier.write(pos[0] + "\n")
    fichier.close()
#get_top_20_pos(POS)

#Q4)
name_entity_list = []
uname_entity_list = []
for entity in global_list:
    if(entity[2] == 'O'):
        uname_entity_list += [entity]
    else:
        name_entity_list += [entity]
        
select_name_entity = random.sample(name_entity_list, 100)
select_uname_entity = random.sample(uname_entity_list, 100)

def store_json_v1(entity_list, filename):
    data = [] 
    
    for entity in entity_list:
        if entity[3]-1 < 0:
            prev = ('null', 'null', 'null' , -1)
        else:
            prev = global_list[entity[3]-1]
        
        if entity[3]+1 >= len(global_list):
            nxt = ('null', 'null', 'null' , -1)
        else:
            nxt = global_list[entity[3]+1]
        
        obj = {}
        obj["previous"] = prev
        obj["current"] = entity
        obj["next"] = nxt
        data += [obj]
        
    json_data = json.dumps(data)
    fichier = open(filename + ".json", "a")
    fichier.write(json_data)
    fichier.close()
#store_json_v1(select_name_entity, "name_entity")
#store_json_v1(select_uname_entity, "uname_entity")
        
###############################################################################



